var passport = require('passport'),
    // GoogleStrategy = require('passport-google').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    TwitterStrategy = require('passport-twitter').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    mongoose = require('mongoose');

module.exports = function() {

  var User = mongoose.model('User');

  passport.use(new GoogleStrategy({
    clientID: '287484643400-ot7qphtu6t9ka661hscg1l3ek73lsi4l.apps.googleusercontent.com',
    clientSecret: 'bg-eGGRR9FBBevEdAZyiRGen',
    callbackURL: 'http://localhost:3000/auth/google/callback'
  }, function( accessToken, refreshToken, profile, done ) {

    User.findOrCreate({ "_google_id": profile.id }, { "name": profile.displayName }, callback);

    function callback( error, user ) {

      if ( error ) {

        return done( error );

      }

      return done(null, user);

    }

  }));

  passport.use(new TwitterStrategy({
    consumerKey: 'CIdni6HS7oGrkrVzZLT4egkpU',
    consumerSecret: 'y9EVIksI1BlwDzIUtjxLnlSjXl6pxza2sbos4PZjVbRr1VKf7j',
    callbackURL: 'http://localhost:3000/auth/twitter/callback'
  }, function( topken, tokenSecret, profile, done ) {

    User.findOrCreate({ "_twitter_id": profile.id }, { "name": profile.displayName }, callback);

    function callback( error, user ) {

      if ( error ) {

        return done( error );

      }

      return done(null, user);

    }

  }));

  passport.use(new FacebookStrategy({
    clientID: '314285112103440',
    clientSecret: '54689c6da90d8e87d2f6db8e60114d37',
    callbackURL: 'http://localhost:3000/auth/facebook/callback'
  }, function( accessToken, refreshToken, profile, done ) {

    User.findOrCreate({ "_facebook_id": profile.id }, { "name": profile.displayName }, callback);

    function callback( error, user ) {

      if ( error ) {

        return done( error );

      }

      return done(null, user);

    }

  }));

  passport.serializeUser(function(user, done) {
    done(null, user._id);
  });

  passport.deserializeUser(function( id, done ) {
    User.findById( id ).exec()
    .then(function( user ) {
      done(null, user);
    });
  });

};

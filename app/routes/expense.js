module.exports = function( app ) {

  var controller = app.controllers.expense,
      checkAuthenticated = require('../../config/auth');

  app.route('/api/expenses')
  .get(checkAuthenticated, controller.index)
  .post(checkAuthenticated, controller.create);

  app.route('/api/expenses/:id')
  .get(checkAuthenticated, controller.show)
  .put(checkAuthenticated, controller.update)
  .delete(checkAuthenticated, controller.delete);

};

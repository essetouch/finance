module.exports = function( app ) {

  var controller = app.controllers.category,
      checkAuthenticated = require('../../config/auth');

  app.route('/api/categories')
  .get(checkAuthenticated, controller.index)
  .post(checkAuthenticated, controller.create);

  app.route('/api/categories/:id/expenses')
  .get(checkAuthenticated, controller.expenses);

  app.route('/api/categories/:id')
  .get(checkAuthenticated, controller.show)
  .put(checkAuthenticated, controller.update)
  .delete(checkAuthenticated, controller.delete);

};

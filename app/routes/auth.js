module.exports = function( app ) {

  var passport = require('passport');

  app.get('/auth/google', passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.profile'] }));
  app.get('/auth/google/callback', passport.authenticate('google', { successRedirect: '/' }));

  app.get('/auth/twitter', passport.authenticate('twitter'));
  app.get('/auth/twitter/callback', passport.authenticate('twitter', { successRedirect: '/' }));

  app.get('/auth/facebook', passport.authenticate('facebook'));
  app.get('/auth/facebook/callback', passport.authenticate('facebook', { successRedirect: '/' }));

  app.get('/auth/logout', function( req, res ) {

    req.logOut();
    res.redirect('/');

  });

};

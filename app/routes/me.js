module.exports = function(app) {

  var controller = app.controllers.me,
      checkAuthenticated = require('../../config/auth');

  app.route('/api/me')
  .get(checkAuthenticated, controller.index);

};
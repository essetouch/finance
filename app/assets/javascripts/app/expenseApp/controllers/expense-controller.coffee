@expenseApp.controller 'ExpenseController', ($scope, $routeParams, $location, $route, Expense, Category)->
  $scope.getExpenses = ->
    $scope.expenses = Expense.query()

  $scope.getExpense = ->
    Expense.get({ id: $routeParams.id }).$promise.then (response)->
      $scope.expense = response

  $scope.getCategories = ->
    $scope.categories = Category.query()

  $scope.addExpense = ->
    Expense.save($scope.expense).$promise.then (response)->
      $route.reload()

  $scope.updateExpense = ->
    Expense.update { id: $routeParams.id }, $scope.expense, (response)->
      $location.path('/expenses')

  $scope.deleteExpense = ( index, expense )->
    Expense.delete({ id: expense._id }).$promise.then (response)->
      $scope.expenses.splice(index, 1)

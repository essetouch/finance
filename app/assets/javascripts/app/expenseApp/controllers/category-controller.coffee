@expenseApp.controller 'CategoryController', ($scope, $routeParams, $location, Category)->
  $scope.showEdit = false

  $scope.getCategories = ->
    $scope.categories = Category.query()

  $scope.getCategory = ->
    Category.get({ id: $routeParams.id }).$promise.then (response)->
      $scope.category = response

  $scope.getExpenses = ->
    Category.expenses { id: $routeParams.id }, (response)->
      $scope.expenses = response

  $scope.addCategory = ->
    Category.save($scope.category).$promise.then (response)->
      $scope.category.name = ''
      $scope.categories.push response

  $scope.updateCategory = (index, category)->
    Category.update { id: category._id }, category, (response)->
      $scope.categories[index] = response
      $scope.showEdit = false

  $scope.deleteCategory = ( index, category )->
    Category.delete({ id: category._id }).$promise.then (response)->
      $scope.categories.splice(index, 1)

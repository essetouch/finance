@expenseApp.factory 'Category', ( $resource )->
  $resource "api/categories/:id/:action", { id: "@id", action: "@action" },
    {
      expenses: {
        method: 'GET',
        params: { action: 'expenses' },
        isArray: true
      },
      update: {
        method: 'PUT',
        isArray: false
      }
    }

@expenseApp.factory 'Expense', ($resource)->
  $resource 'api/expenses/:id', { id: '@id' },
    update: {
      method: 'PUT',
      isArray: false
    }

@expenseApp.config ($routeProvider, $locationProvider, view_path)->

  # $locationProvider.html5Mode(true).hashPrefix('!')

  $routeProvider
    .when "/",
      templateUrl: "#{view_path}/home/index.html",
      controller: "HomeController"
      title: "App"

    .when "/categories",
      templateUrl: "#{view_path}/categories/index.html",
      controller: "CategoryController",
      title: "Categories"

    .when "/expenses",
      templateUrl: "#{view_path}/expenses/index.html",
      controller: "ExpenseController",
      title: "Expenses"

    .when "/expenses/:id/edit",
      templateUrl: "#{view_path}/expenses/edit.html",
      controller: "ExpenseController",
      title: "Expenses Edit"

    .when "/404",
      templateUrl: "#{view_path}/errors/404.html",
      title: "404"

    .otherwise
      redirectTo: "/404"

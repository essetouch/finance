@expenseApp.constant "view_path", "views"
@expenseApp.constant "directive_view_path", "views/directives"

@expenseApp.run ($rootScope, $route, $location)->

  if window.location.hash == '#/_=_' or window.location.hash == '#_=_'
    $location.path('/')

  $rootScope.$on "$routeChangeStart", (event, currRoute, prevRoute)->
    $rootScope.title = currRoute.title
    $('body, html').scrollTop(0)

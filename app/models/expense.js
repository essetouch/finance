var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

module.exports = function() {

  var schema = Schema({
    _user_id: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    _category_id: {
      type: Schema.Types.ObjectId,
      ref: 'Category'
    },
    name: {
      type: String
    },
    cost: {
      type: String
    },
    date: {
      type: Date
    },
    repeat: {
      type: String
    }
  });

  return mongoose.model('Expense', schema);

};
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

module.exports = function() {

  var schema = Schema({
    _user_id: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    name: {
      type: String
    },
    expenses: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Expense'
      }
    ]
  });

  return mongoose.model('Category', schema);

};

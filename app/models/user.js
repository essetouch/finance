var mongoose = require('mongoose'),
    findOrCreate = require('mongoose-findorcreate'),
    Schema = mongoose.Schema;

module.exports = function() {

  var schema = Schema({
    name: {
      type: String
    },
    gender: {
      type: String
    },
    dateOfBirth: {
      type: Date
    },
    email: {
      type: String
    },
    _google_id: {
      type: String
    },
    _facebook_id: {
      type: String
    },
    _twitter_id: {
      type: String
    },
    categories: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Category'
      }
    ],
    expenses: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Expense'
      }
    ]
  });

  schema.plugin( findOrCreate );

  return mongoose.model('User', schema);

};
module.exports = function( app ) {

  var controller = {},
      Expense = app.models.expense;

  controller.index = function( req, res ) {

    Expense.find({ _user_id: req.user.id }).populate('_category_id').exec( callback );
    // .then( success, error );

    function callback( error, expenses ) {

      if ( error ) res.status( 500 ).json( error );

      console.log(expenses);

      res.json( expenses );

    }

    function success( expenses ) {

      if ( !expenses ) throw new Error("Expense not found");

      res.json( expenses );

    }

    function error( error ) {

      res.status( 500 ).json( error );

    }

  };

  controller.show = function( req, res ) {

    Expense.findOne({ _id: req.params.id, _user_id: req.user.id }).exec( callback );

    function callback( error, expense ) {

      if ( error ) res.status( 500 ).json( error );

      res.json( expense );

    }

  };

  controller.create = function( req, res ) {

    var expense = new Expense({
      _user_id: req.user.id,
      _category_id: req.body._category_id,
      name: req.body.name,
      cost: req.body.cost,
      date: req.body.date,
      repeat: req.body.repeat
    });

    expense.save(function( error, expense ) {

      if ( error ) {

        res.status( 500 ).end();

      } else {

        res.json( expense );

      }

    });

  };

  controller.update = function( req, res ) {

    Expense.findOneAndUpdate({ _id: req.params.id, _user_id: req.user.id }, {
        _category_id: req.body._category_id,
        name: req.body.name,
        cost: req.body.cost,
        date: req.body.date,
        repeat: req.body.repeat
      }, { new: true }, callback);

    function callback( error, expense ) {

      if ( error ) res.status( 500 ).json( error );

      res.json( expense );

    }

  };

  controller.delete = function( req, res ) {

    Expense.findOneAndRemove({ _id: req.params.id, _user_id: req.user.id }, callback);

    function callback( error, expense ) {

      if ( error ) res.status( 500 ).json( error );

      res.json( expense );

    }

  };

  return controller;

};

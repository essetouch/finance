var jbuilder = require('jbuilder');

module.exports = function( app ) {

  var controller = {},
      User = app.models.user;

  controller.index = function(req, res) {

    User.findOne({ _id: req.user.id }).exec( callback );

    function callback( error, user ) {

      if ( error ) res.status( 500 ).json( error );

      var response = jbuilder.create(function( json ) {
        json.set('name', user.name);
        json.set('gender', user.gender);
        json.set('dateOfBirth', user.dateOfBirth);
        json.set('email', user.email);
      });

      res.json( response.content );

    }

  };

  return controller;

};

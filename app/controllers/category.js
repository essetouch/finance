module.exports = function( app ) {

  var controller = {},
      Category = app.models.category,
      Expense = app.models.expense;

  controller.index = function( req, res ) {

    Category.find({ _user_id: req.user.id }).exec().then( success, error );

    function success( categories ) {

      if ( !categories ) throw new Error("Category not found");

      res.json( categories );

    }

    function error( error ) {

      res.status( 500 ).json( error );

    }

  };

  controller.show = function( req, res ) {

    Category.findOne({ _id: req.params.id, _user_id: req.user.id }).exec( callback );

    function callback( error, category ) {

      if ( error ) res.status( 500 ).json( error );

      res.json( category );

    }

  };

  controller.create = function( req, res ) {

    var category = new Category({
      name: req.body.name,
      _user_id: req.user.id
    });

    category.save(function( error, category ) {

      if ( error ) {

        res.status( 500 ).end();

      } else {

        res.json( category );

      }

    });

  };

  controller.update = function( req, res ) {

    Category.findOneAndUpdate({ _id: req.params.id, _user_id: req.user.id }, { name: req.body.name }, { new: true }, callback);

    function callback( error, category ) {

      if ( error ) res.status( 500 ).json( error );

      res.json( category );

    }

  };

  controller.delete = function( req, res ) {

    Category.findOneAndRemove({ _id: req.params.id, _user_id: req.user.id }, callback);

    function callback( error, category ) {

      if ( error ) res.status( 500 ).json( error );

      res.json( category );

    }

  };

  controller.expenses = function( req, res ) {

    Expense.find({ _category_id: req.params.id, _user_id: req.user.id }).exec().then( success, error );

    function success( expenses ) {

      if ( !expenses ) throw new Error("Expense not found");

      res.json( expenses );

    }

    function error( error ) {

      res.status( 500 ).json( error );

    }

  };

  return controller;

};

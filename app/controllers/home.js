module.exports = function( app ) {

  var controller = {};

  controller.index = function( req, res, next ) {

    if ( req.isAuthenticated() ) {

      var options = {
        root: '../public/views/layouts/',
        dotfiles: 'deny',
        headers: {
          'x-timestamp': Date.now(),
          'x-sent': true
        }
      };

      res.sendFile('index.html', options);

    } else {

      res.render('auth');

    }

  };

  return controller;

};

module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  var PATH = {
    assets: {
      stylesheets: 'app/assets/stylesheets/**/*.scss',
      javascripts: {
        app: 'app/assets/javascripts/app/**/*.coffee',
        libs: {
          'public/javascripts/libs/plugins.js': [
            'app/assets/javascripts/libs/plugins/**/*.js'
          ],
          'public/javascripts/libs/angular.js': [
            'app/assets/javascripts/libs/angular/**/*.js'
          ],
          'public/javascripts/libs/structure.js': [
            'app/assets/javascripts/libs/old/structure/**/*.js'
          ],
          'public/javascripts/libs/functionality.js': [
            'app/assets/javascripts/libs/old/functionality/**/*.js'
          ]
        }
      }
    },
    public: {
      views: 'public/views',
      javascripts: 'public/javascripts',
      stylesheets: 'public/stylesheets',
      images: 'public/images'
    },
    app: {
      root: './app'
    }
  };

  grunt.initConfig({

      PATH: PATH,

      compass: {
        dist: {
          options: {
            config: 'config.rb'
          }
        }
      },

      coffee: {
        compileJoined: {
          options: {
            join: true
          },
          files: {
            '<%= PATH.public.javascripts %>/application.js': ['<%= PATH.assets.javascripts.app %>']
          }
        }
      },

      // uglify: {
      //     my_target: {
      //         files: '<%= PATH.dist.js %>'
      //     }
      // },

      concat: {
        application_and_libs: {
          files: '<%= PATH.assets.javascripts.libs %>'
        }
      },

      // htmlmin: {
      //     dist: {
      //         options: {
      //             collapseWhitespace: true,
      //             removeRedundantAttributes: true,
      //             useShortDoctype: true,
      //             removeComments: true
      //         },

      //         files: [{
      //             expand: true,
      //             cwd: '<%= PATH.dist.root %>',
      //             src: '{,*/}*.html',
      //             dest: '<%= PATH.dist.root %>'
      //         }]
      //     }
      // },

      // imagemin: {
      //     dynamic: {
      //         files: [{
      //             expand: true,
      //             cwd: '<%= PATH.dist.img %>',
      //             src: ['**/*.{png,jpg,gif}'],
      //             dest: '<%= PATH.dist.img %>'
      //         }]
      //     }
      // },

      concurrent: {
          dev: ['compass', 'coffee', 'concat'],
          // run: ['uglify', 'imagemin', 'htmlmin']
      },

      // connect: {
      //     options: {
      //         port: 3000,
      //         open: true,
      //         // livereload: 35729,
      //         hostname: 'localhost',
      //         base: '<%= PATH.app.root %>'
      //     },

      //     livereload: {
      //         options: {
      //             middleware: function( connect ) {
      //                 return [
      //                     connect.static(PATH.public.views),
      //                     connect.static(PATH.public.stylesheets),
      //                     connect.static(PATH.public.javascripts),
      //                     connect.static(PATH.public.images)
      //                 ];
      //             }
      //         }
      //     }
      // },

      // connect: {
      //   server: {
      //     options: {
      //       port: 3000,
      //       open: true,
      //       hostname: 'localhost',
      //       onCreateServer: function(server, connect, options) {
      //         var io = require('socket.io').listen(server);
      //         io.sockets.on('connection', function(socket) {
      //           // do something with socket
      //         });
      //       }
      //     }
      //   }
      // },

      watch: {
          css: {
              files: '<%= PATH.assets.stylesheets %>',
              tasks: ['compass']
          },

          scripts: {
              files: '<%= PATH.assets.javascripts.app %>',
              tasks: ['coffee']
          },

          html: {
            files: '**/*.html',
            options: {
              livereload: true
            }
          },

          options: {
            livereload: true
          }

          // livereload: {
          //     options: {
          //         livereload: '<%= connect.options.livereload %>'
          //     },

          //     files: [
          //         '<%= PATH.public.views %>/{,*/}*.html',
          //         '<%= PATH.public.stylesheets %>/{,*/}*.css',
          //         '<%= PATH.public.javascripts %>/{,*/}*.js',
          //         '<%= PATH.public.images %>/{,*/}*'
          //     ]
          // }
      }

      // copy: {
      //     dist: {
      //         expand: true,
      //         src: '<%= PATH.app.root %>/**',
      //         dest: '<%= PATH.dist.root %>'
      //     }
      // },

      // clean: {
      //     dist: ['<%= PATH.dist.img %>/tmp/', '<%= PATH.dist.root %>/app/source/']
      // }

  });

  grunt.registerTask('default', ['concurrent:dev', 'watch']);
  // grunt.registerTask('run', ['compass', 'copy', 'concurrent:run', 'clean']);

};
(function() {
  this.expenseApp = angular.module('expenseApp', ['ngRoute', 'ngResource']);

  this.expenseApp.constant("view_path", "views");

  this.expenseApp.constant("directive_view_path", "views/directives");

  this.expenseApp.run(function($rootScope, $route, $location) {
    if (window.location.hash === '#/_=_' || window.location.hash === '#_=_') {
      $location.path('/');
    }
    return $rootScope.$on("$routeChangeStart", function(event, currRoute, prevRoute) {
      $rootScope.title = currRoute.title;
      return $('body, html').scrollTop(0);
    });
  });

  this.expenseApp.config(function($routeProvider, $locationProvider, view_path) {
    return $routeProvider.when("/", {
      templateUrl: "" + view_path + "/home/index.html",
      controller: "HomeController",
      title: "App"
    }).when("/categories", {
      templateUrl: "" + view_path + "/categories/index.html",
      controller: "CategoryController",
      title: "Categories"
    }).when("/expenses", {
      templateUrl: "" + view_path + "/expenses/index.html",
      controller: "ExpenseController",
      title: "Expenses"
    }).when("/expenses/:id/edit", {
      templateUrl: "" + view_path + "/expenses/edit.html",
      controller: "ExpenseController",
      title: "Expenses Edit"
    }).when("/404", {
      templateUrl: "" + view_path + "/errors/404.html",
      title: "404"
    }).otherwise({
      redirectTo: "/404"
    });
  });

  this.expenseApp.controller('CategoryController', function($scope, $routeParams, $location, Category) {
    $scope.showEdit = false;
    $scope.getCategories = function() {
      return $scope.categories = Category.query();
    };
    $scope.getCategory = function() {
      return Category.get({
        id: $routeParams.id
      }).$promise.then(function(response) {
        return $scope.category = response;
      });
    };
    $scope.getExpenses = function() {
      return Category.expenses({
        id: $routeParams.id
      }, function(response) {
        return $scope.expenses = response;
      });
    };
    $scope.addCategory = function() {
      return Category.save($scope.category).$promise.then(function(response) {
        $scope.category.name = '';
        return $scope.categories.push(response);
      });
    };
    $scope.updateCategory = function(index, category) {
      return Category.update({
        id: category._id
      }, category, function(response) {
        $scope.categories[index] = response;
        return $scope.showEdit = false;
      });
    };
    return $scope.deleteCategory = function(index, category) {
      return Category["delete"]({
        id: category._id
      }).$promise.then(function(response) {
        return $scope.categories.splice(index, 1);
      });
    };
  });

  this.expenseApp.controller('ExpenseController', function($scope, $routeParams, $location, $route, Expense, Category) {
    $scope.getExpenses = function() {
      return $scope.expenses = Expense.query();
    };
    $scope.getExpense = function() {
      return Expense.get({
        id: $routeParams.id
      }).$promise.then(function(response) {
        return $scope.expense = response;
      });
    };
    $scope.getCategories = function() {
      return $scope.categories = Category.query();
    };
    $scope.addExpense = function() {
      return Expense.save($scope.expense).$promise.then(function(response) {
        return $route.reload();
      });
    };
    $scope.updateExpense = function() {
      return Expense.update({
        id: $routeParams.id
      }, $scope.expense, function(response) {
        return $location.path('/expenses');
      });
    };
    return $scope.deleteExpense = function(index, expense) {
      return Expense["delete"]({
        id: expense._id
      }).$promise.then(function(response) {
        return $scope.expenses.splice(index, 1);
      });
    };
  });

  this.expenseApp.controller('HomeController', function($rootScope, Me) {
    return $rootScope.me = Me.get();
  });

  this.expenseApp.factory('Category', function($resource) {
    return $resource("api/categories/:id/:action", {
      id: "@id",
      action: "@action"
    }, {
      expenses: {
        method: 'GET',
        params: {
          action: 'expenses'
        },
        isArray: true
      },
      update: {
        method: 'PUT',
        isArray: false
      }
    });
  });

  this.expenseApp.factory('Expense', function($resource) {
    return $resource('api/expenses/:id', {
      id: '@id'
    }, {
      update: {
        method: 'PUT',
        isArray: false
      }
    });
  });

  this.expenseApp.factory('Me', function($resource) {
    return $resource('api/me');
  });

}).call(this);
